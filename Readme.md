#Front-end Engineer Technical Test

**Requirements:**

-Download Prototype Design file FE-PracticalTest.xd. There are 4 test screens in the prototype design. Pick 1 of the test screens that you prefer. 

-This file can be opened using Adobe XD (Free). Use Adobe XD to export asset matertials as well as check design specs.

-Use one of the following to build the test screen
	-Angular
	-React 
	
-Commit your code to github / bitbucket and share with us your repo url.
